import smbus2 as smbus
import math

class LaserDriver:

        def __init__(self, addr = 0x60):
                self.dev_addr = addr

                self.intensity = 0
                self.rawValue = 0

                self.defaultValue = 60
            
                self.i2c = smbus.SMBus(1)

        def setIntensity(self, percent):
                if percent > 0 and percent < 100:
                        self.intensity = percent
                        DAC_value = math.floor((percent/330)*4095)
                        self.sendData(DAC_value)
                else:
                        self.sendData(0)
        def turnOn(self):
            self.setIntensity(self.defaultValue)
        
        def turnOff(self):
            self.setIntensity(0)
        
        def setDefaultValue(self, val):
            self.defaultValue = val

        def getIntensity(self):
                return self.intensity

        def sendData(self, data):
                data = data.to_bytes(2, 'big')
                self.i2c.write_byte_data(self.dev_addr, data[0], data[1])