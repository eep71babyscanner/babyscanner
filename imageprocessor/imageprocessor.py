import numpy as np
import cv2
from stl import mesh
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.tri as triang

class ScanProcessor:

    def __init__(self, rpc=0.0004, ro=0.2056,d_cam_laser = 8, d_laser_surface = 38.5):
        self.rpc = rpc
        self.ro = ro
        self.d_cam_laser = d_cam_laser
        self.d_laser_surface = d_laser_surface

        self.crop_y_start = 0
        self.crop_y_end = 300

        self.crop_x_start = 0
        self.crop_x_end = 1250

        self.rotation = 0
        self._filterThres = 70

        self.mtx = np.load('/home/pi/kai/babyscanner/imageprocessor/paramMTX.npy')
        self.dist = np.load('/home/pi/kai/babyscanner/imageprocessor/paramDIST.npy')

        self.mtx[0][2] = 400
        self.mtx[0][0] = 1500
        self.mtx[1][2] = 200
        self.mtx[2][2] = 0.9

        self.pixel_distance = None

        self.bed_width = 17.9

        self.xz_figure = None

    def setCropY(self, start, end):
        self.crop_y_start = start
        self.crop_y_end = end
    
    def setCropX(self, start, end):
        self.crop_x_start = start
        self.crop_x_end = end

    def setCameraRotation(self, rotation):
        self.rotation = rotation

    def crop(self, image):
        height, width = image.shape[:2]
        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(self.mtx, self.dist, (width,height), 1, (width,height))
        mapx, mapy = cv2.initUndistortRectifyMap(self.mtx, self.dist, None, newcameramtx, (width, height), 5)

        image = cv2.remap(image, mapx, mapy, cv2.INTER_LINEAR)
        
        img = image[self.crop_y_start:self.crop_y_end, self.crop_x_start:self.crop_x_end]
        (h, w) = img.shape[:2]
        #calculate the center of the image
        center = (w / 2, h / 2)
        M = cv2.getRotationMatrix2D(center, 180 - self.rotation, 1.0)
        img = cv2.warpAffine(img, M, (w,h))

        return img

    def setFilterThres(self, thres):
        self._filterThres = thres
    
    def getFilterThres(self):
        return self._filterThres
    def filter(self, image):
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, img2 = cv2.threshold(img, self._filterThres, 255, cv2.THRESH_BINARY)
        ret, img2 = cv2.threshold(img2, 0, 255, cv2.THRESH_OTSU)

        return img2
    
    def running_mean(self, a, n):
        cumsum = np.cumsum(np.insert(a,0,0))
        return (cumsum[n:] - cumsum[:-n]) / float(n)
    
    def calcYZ(self, image):
        img2 = self.filter(image)
        # Get pixel locations of where the laser is in the image
        laser_indices = np.argwhere(img2 == 255)

        # Sort the pixel locations
        laser_indices = laser_indices[laser_indices[:,1].argsort()]

        li_unique = np.unique(laser_indices[:,1], return_index = True, return_counts=True)
        li_first = laser_indices[li_unique[1],:]

        li_idx_last = (li_unique[1] + li_unique[2])
        li_idx_last[-1] = li_idx_last[-1] -1

        li_last = laser_indices[li_idx_last,:]

        li_new = (li_first + li_last) / 2

        y_val = -1 * li_first[:,0]
        x_val = li_first[:,1]

        # Calculate the reference line where Z = 0 with linear coefficients
        a = (y_val[-1] - y_val[0]) / (x_val[-1] - x_val[0])
        b = (y_val[0]) - (a * x_val[0])
        ref_line = (a * x_val) + b

        # Count the pixels between measured line and reference line
        pfc = y_val - ref_line

        #N = 3
        #pfc = np.convolve(pfc, np.ones((N,))/N, mode='same')

        self.pixel_distance = pfc

        # Calculate the angle
        theta = self.rpc * abs(pfc) + self.ro
        tan_theta = np.tan(theta)

        # Calculate the height (z) with the calculated angle
        z = (self.d_laser_surface - (self.d_cam_laser / tan_theta)) * 10
        z[z < 0] = 0

        z_max = z.max()
        y = (x_val * (self.bed_width/1280)) * 10
        

        yz = np.c_[y,z]

        points_to_add = (self.crop_x_end - self.crop_x_start) - len(yz)
        extra_points = np.zeros((2,points_to_add))

        yz = yz.transpose()
        yz = np.hstack((yz, extra_points))
        yz = yz.transpose()
        return yz, z_max

    def render3D(self, xyz, exportSTL=True):
        x = xyz[:,0]
        y = xyz[:,1]
        z = xyz[:,2]

        tri= triang.Triangulation(x,y)
        #figure = plt.figure(figsize=(20,15))
        #ax = figure.add_subplot(111, projection='3d')
        #ax.view_init(elev=70., azim=225)
        #surf = ax.plot_trisurf(x, y, z, triangles=tri.triangles,cmap=cm.summer, antialiased=False, shade=True)
        #figure.savefig("/home/pi/baby3D.png")

        if exportSTL == True:
            print('stl export')
            #self.getXZView(xyz, 1250)
            self.exportToSTL(x,y,z,tri)

        return
    
    def exportToSTL(self, x, y, z, tri):
        data = np.zeros(len(tri.triangles), dtype=mesh.Mesh.dtype)
        babyMesh = mesh.Mesh(data, remove_empty_areas=False)
        babyMesh.x[:] = x[tri.triangles]
        babyMesh.y[:] = y[tri.triangles]
        babyMesh.z[:] = z[tri.triangles]
        babyMesh.save("/home/pi/baby.stl")
        return    
    
    def plotXZ(self, xz):
        x = xz[:,0]
        z = xz[:,1]

        figure = plt.figure(figsize=(8.34,2))
        scatter = plt.scatter(x, z)
        figure.savefig('/home/pi/hoogteprofiel.png')
        self.xz_figure = figure

        return

    def getXZView(self, xyz, sliceWidth):
        x = xyz[:,0]
        x = np.unique(x)
        z = xyz[:,2].reshape(sliceWidth,-1)
        zmax=np.zeros(shape=(z[0,:].size))
        xz=np.zeros(shape=(z[0,:].size,2))       
        for i in range(z[0,:].size):
        	temp=z[i,:]
        	zmax[i]=temp.max()
        	xz[i,0]=x[i]
        	xz[i,1]=zmax[i]     
        figure = plt.figure(figsize=(8.34,2))
        scatter = plt.scatter(x,zmax)
        figure.savefig('/home/pi/hoogteprofiel.png')
        self.xz_figure = figure
        #plt.show()
        return xz

#def measurement_to_grid(points):
#    # process scanned data to reference lines
#
#    x = points[0,:]
#    y = points[1,:]
#    z = points[2,:]
#
#    slice_width = 10 # mm
#
#    # take the reference profile a bit from the edge
#    offset_from_edge = 10 # mm
#
#    # create mask
#    slice_1_mask = (x >= x.min() + offset_from_edge) & (x <= x.min() + offset_from_edge + slice_width)
#    slice_2_mask = (x <= x.max() - offset_from_edge) & (x >= x.max() - offset_from_edge - slice_width)
#
#    # apply mask
#    slice_1 = points[:,slice_1_mask]
#    slice_2 = points[:,slice_2_mask]
#
#    # sort values on y
#    slice_1 = slice_1[:,slice_1[1,:].argsort()]
#    slice_2 = slice_2[:,slice_2[1,:].argsort()]
#
#    # cut off values lower than the cutoff value
#    cutoff = -60
#    slice_1 = slice_1[:,slice_1[2,:] >= cutoff]
#    slice_2 = slice_2[:,slice_2[2,:] >= cutoff]
#
#    # fit polynomial
#    order = 10
#    ref_1_fit = np.polyfit(slice_1[1,:],slice_1[2,:], order)
#    ref_2_fit = np.polyfit(slice_2[1,:],slice_2[2,:], order)
#
#    min_y = min([slice_1[1,:].min(),slice_2[1,:].min()])
#    max_y = min([slice_1[1,:].max(),slice_2[1,:].max()])
#
#    min_x = min([slice_1[0,:].min(),slice_2[0,:].min()])
#    max_x = min([slice_1[0,:].max(),slice_2[0,:].max()])
#
#    # take the mean x value of the slices as their x location
#    x_slice_1 = slice_1[0,:].mean()
#    x_slice_2 = slice_2[0,:].mean()
#
#    nx = cf.grid_n_x # data points in x direction on grid
#    x_grid = np.linspace(x_slice_1,x_slice_2,nx)
#
#    ny = cf.grid_n_y # data points in y direction on grid
#    y_grid = np.linspace(min_y,max_y,ny)
#
#    xx,yy = np.meshgrid(x_grid,y_grid,indexing = 'ij')
#
#    # measurement grid
#    rail_xy = np.empty(shape = (points.shape[1],2))
#    rail_xy[:,0] = points[0,:]
#    rail_xy[:,1] = points[1,:]
#    rail_z = points[2,:]
#
#    rail_z = griddata(rail_xy,rail_z,(xx,yy),method='linear')
#
#    rail_grid = Grid(rail_z.shape)
#    rail_grid.x[:,:] = xx
#    rail_grid.y[:,:] = yy
#    rail_grid.z[:,:] = rail_z
#    return rail_grid,ref_grid      
##    