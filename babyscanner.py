from camera import camera as cam
from motorcontrol import motorcontrol as mc
from laserdriver import laserdriver as ld
from imageprocessor import imageprocessor as improc

import configuration as cf

from threading import Thread
import time

import numpy as np
from collections import deque

cycle_time = 0.01

class BabyScannerApp():

    def __init__(self, head = True, motor = True):

        self.head = head

        if self.head is True:
            from GUI import UserInterface as ui
        else:
            from GUI import UserInterfaceStub as ui

        self.uiHandler = ui.UserInterface()
        self.camHandler = cam.CameraStream()
        self.laserHandler = ld.LaserDriver()

        self.proc = improc.ScanProcessor()

        self.motor = motor
        if self.motor is True:
            
            from motorcontrol import motorcontrol as mc
        else:
            from motorcontrol import motorcontrolstub as mc
        
        self.motorHandler = mc.MotorController('/dev/ttyAMA0', 4800)
        self.motorHandler.set_gear(1/cf.distance_per_step_x)


        self.uiHandler.window.setStopFunction(self.stopFunction)

        self._events = {
            'boot_success': False,
            'on_calibrate_system': False,
            'on_start_scan': False, 
            'on_reset_system': False,
            'on_export_stl': False,
            'calibration_completed': False,
            'scan_completed': False,
            'export_completed': False,
            'on_bright_high': False,
            'on_bright_low': False,
            'on_contrast_high': False,
            'on_contrast_low': False,
            'on_laser_high': False,
            'on_laser_low': False,
            }

        self._op_state = 'pre-boot'
        self._last_state = ""
        self._next_state = 'idle'

        self.movement_started = False
        self.movement_completed = False

        self._connect_started = False
        self._connect_complete = False

        self._scan_started = False
        self._export_started = False
        self._terminate_scan = False
        self._scanned_successfully = False
        self._export_success = False
        self._calibStarted = False

        self.queueA = deque(maxlen = 5000)
        self.queueB = deque(maxlen = 5000)

        self._scanStart = False
        self._calibStart = False
        self._exportStart = False
        self._homeStart = False

        self._x_start = 30.0
        self._x_end = 500.0

        self.xyz = None
        self.xz = np.zeros((2,0))

        self.baby_length = 0 

    def start_control_loop(self):
        t = Thread(target=self._control_loop)
        t.daemon = True
        t.start()

    def _control_loop(self):

        self.motorHandler.connect()
        self.uiHandler.setLength(12.5)
        '''
        This is the control loop that handles all the states and transitions
        within the system.

        Operating states
            * idle 
                The system shows the userinterface with camera feedback and waits for
                user input.
            * calibrating
                The system positions the scan device to its start/home position and sets the laser
                focus.
            * scanning
                The system moves the scan device over the scan range, retrieves images from the camera
                every time the motors has moved N steps. Simulteanously every retrieved image is taken from
                the buffer and processed to get 3D information.
            * exporting
                The system exports the latest 3D scan to STL and sends it to a server via FTP
        
        Events that cause state transitions
            - calibrate_button_pressed: idle        -> calibrating     
            - calibration_finished:     calibrating -> idle
            - scan_button_pressed:      idle        -> scanning
            - scan_finished:            scanning    -> idle
            - export_button_pressed:    idle        -> exporting 
            - export_finished:          exporting   -> idle   
        
        The speed of the control loop is throttled with cycle time. 
        '''
        t_start = time.perf_counter()
        t_end = time.perf_counter()
        cycle_warnings = 0
        

        while True:
            # Determine the time that the cpu needs to sleep for throttling
            sleep_time = cycle_time - (t_end - t_start)
            if sleep_time > 0:
                time.sleep(sleep_time)
            
            t_start = time.perf_counter()

            # Send the current state to the UI
            if self._op_state != self._next_state:
                self.uiHandler.setStatus(self._next_state)
            

            # Check what the next state needs to be based on the events
            self._op_state = self._next_state
            events = self.getEvents()
            
            if self._last_state != self._op_state:
                self._last_state = self._op_state
                print("state: " + self._op_state)

            #if self._op_state == 'pre_boot':

            #    if self._connect_started is False:
            #        self.start_connect_routine()
            #        self._connect_started = True
                
            #    if events['boot_success'] is True:
            #        self._next_state = 'idle'

            if self._op_state == 'idle':

                #if events['on_bright_high']:
                #    brightness = self.camHandler.getBrightness()
                #    self.camHandler.setBrightness(brightness + 0.02)
                
                #if events['on_bright_low']:
                #    brightness = self.camHandler.getBrightness()
                #    self.camHandler.setBrightness(brightness - 0.02)
                
                #if events['on_contrast_high']:
                #    contrast = self.camHandler.getContrast()
                #    self.camHandler.setContrast(contrast + 0.02)
                #
                #if events['on_contrast_low']:
                #    contrast = self.camHandler.getContrast()
                #    self.camHandler.setContrast(contrast - 0.02)
                #
                #if events['on_laser_high']:
                #    intensity = self.laserHandler.getIntensity()
                #    self.laserHandler.setIntensity(intensity + 1)
                #
                #if events['on_laser_low']: 
                #    intensity = self.laserHandler.getIntensity()
                #    self.laserHandler.setIntensity(intensity - 1)
    #
                if events['on_calibrate_system']:
                    self._next_state = 'calibrating'

                if events['on_start_scan']:
                    print('scan triggered')
                    self._next_state = 'scanning'
                
                if events['on_export_stl']:
                    self._next_state = 'exporting'
                
            if self._op_state == 'calibrating':
                if self._calibStarted is False:
                    
                    self.motorHandler.start_homing_routine()
                    self._calibStarted = True

                if events['calibration_completed']:
                    self._calibStarted = False
                    self._next_state = 'idle'
            
            if self._op_state == 'scanning':
                if self._scan_started is False:
                    self.start_scan_routine()
                    self._scan_started = True

                if events['scan_completed']:

                    self._scan_started = False
                    self._scanned_successfully = False
                    self._next_state = 'exporting'
                
            if self._op_state == 'exporting':
                if self._export_started is False:
                    self.start_export_routine()
                    self._export_started = True 

                if events['export_completed']:
                    self._export_started = False
                    self._next_state = 'idle'
            
            if events['on_reset_system']:
                if self._scan_started == True:
                    self._terminate_scan = True
                    self._scan_started = False
                
                self._next_state = 'idle'

            t_end = time.perf_counter()
            if ((t_end - t_start) > cycle_time):
                cycle_warnings += 1
                print('WARNING: cycle execution time exceeds cycle_time: %i times with %i' %(cycle_warnings,int((t_end - t_start)*1000)))

    def start(self):
        self.camHandler.start()
        time.sleep(0.1)
        
        self.laserHandler.turnOn()

        self.start_update_routine()
        self.start_control_loop()

        # Has to be started as last
        self.uiHandler.start()
    
    def start_update_routine(self):
        t = Thread(target=self.updateCameraUi)
        t.daemon = True
        t.start()

    def updateCameraUi(self):
        '''
         Feed the uiHandler images from the camHandler so the camera can be viewed in the ui
        '''
        while True:
            if self.uiHandler.window.name is 'main':
                img = self.camHandler.read()
                self.uiHandler.window.displayImage(img)
            else:
                pass

            time.sleep(0.1)
    
    def start_scan_routine(self):
        t = Thread(target=self.go_scan)
        t.start()

        return True

    def start_connect_routine(self):
        t = Thread(target=self.connect_motor)
        t.start()

    def connect_motor(self):
        print('start connecting')
        self.motorHandler.connect()
        self._connect_complete = True

    def stopFunction(self):
        if self.motorHandler.isConnected:
            self.motorHandler.disconnect()

        self.camHandler.stop()

    def start_send_home_routine(self):
        t = Thread(target=self.send_home)
        t.start()

        return True

    def send_home(self):

        self.motorHandler.move_metric(0, 15)
        while not self.motorHandler.on_target():
            time.sleep(0.01)
        
        self.movement_completed = True
        self.movement_started = False

    def setScanDistance(self, x_start, x_end):
        self._x_start = x_start
        self._x_end = x_end
    
    def getPoints(self, x_m):
        frame = self.camHandler.read()
        img = self.proc.crop(frame)
        yz, z_max = self.proc.calcYZ(img)
        x = np.full(len(yz), x_m)

        xz = np.zeros((2,1))
        xz[0,] = x_m
        xz[1,] = z_max

        self.xz = np.hstack((self.xz, xz))

        xyz = np.c_[x, yz]

        return xyz
    
    def calculateLength(self, xz):
        idx = np.argwhere(xz[:,1] > 40)
        xz_filtered = xz[idx]
        xz_filtered = xz_filtered.transpose()

        length = xz_filtered[0,:].max() - xz_filtered[0,:].min()

        self.baby_length = length

        return

    def go_scan(self):
        self.motorHandler.move_metric(self._x_start, 40)
        while not self.motorHandler.on_target():
            time.sleep(0.01)
        
        x_interval = 0.2
        x_now =  0
        x_prev = 0


        points = self.getPoints(0)
        points = points.transpose()

        self.motorHandler.move_metric(self._x_end, 200)
        start = time.perf_counter()
        while not self.motorHandler.on_target():
            
            x_now = self.motorHandler.get_pos_metric()
            if abs((x_now - x_prev)) >= x_interval:
                new_points = self.getPoints(x_now)
                new_points = new_points.transpose()

                points = np.hstack((points, new_points))
                x_prev = x_now
        
        self.xyz = points
        print(self.xyz.shape)

        xz = self.xz.transpose()
        self.proc.plotXZ(xz)
        self.calculateLength(xz)


        end = time.perf_counter()
        elapsed = end - start

        print('scanned succesfully')
        #print(self.queueB)
        #print(len(self.queueB))
        print(elapsed)

        self._scanned_successfully = True

    def start_export_routine(self):
        t = Thread(target=self.go_export)
        t.start()

        return True

    def go_export(self):
        xyz = self.xyz.transpose()

        self.proc.render3D(xyz, True)
        self._export_success = True

    def getEvents(self):
        self._events['scan_completed'] = self.getScanEvent()
        self._events['calibration_completed'] = self.getHomeCompleted()
        self._events['export_completed'] = self.getExportCompleted()
        self._events['boot_success'] = self.getConnectCompleted()

        if self.head is True:
            self._events['on_start_scan'] = self.uiHandler.getStartEvent()
            self._events['on_reset_system'] = self.uiHandler.getStopEvent()
            self._events['on_calibrate_system'] = self.uiHandler.getHomeEvent()
            self._events['on_export_stl'] = self.getExportEvent()

            #self._events['on_bright_high'] = self.uiHandler.getBrightnessHigherEvent()
            #self._events['on_bright_low'] = self.uiHandler.getBrightnessLowerEvent()
            #self._events['on_contrast_high'] = self.uiHandler.getContrastHigherEvent()
            #self._events['on_contrast_low'] = self.uiHandler.getContrastLowerEvent()
            #self._events['on_laser_high'] = self.uiHandler.getLaserHigherEvent()
            #self._events['on_laser_low'] = self.uiHandler.getLaserLowerEvent()

        else:
            self._events['on_start_scan'] = self.getStartEvent()
            self._events['on_reset_system'] = self.getStopEvent()
            self._events['on_calibrate_system'] = self.getHomeEvent()
            self._events['on_export_stl'] = self.getExportEvent()

        return self._events

    def setEvent(self, event):
        if event is 'scan':
            self._scanStart = True
        
        if event is 'export':
            self._exportStart = True
        
        if event is 'stop':
            self._homeStart = True
        
        if event is 'calib':
            self._calibStart = True
        
    def getStartEvent(self):
        event = self._scanStart

        if(self._scanStart is True):
            self._scanStart = False
        
        return event

    def getHomeEvent(self):
        event = self._calibStart

        if(self._calibStart is True):
            self._calibStart = False
        
        return event
    
    def getStopEvent(self):
        event = self._homeStart

        if(self._homeStart is True):
            self._homeStart = False
        
        return event
    
    def getConnectCompleted(self):
        event = self._connect_complete

        if self._connect_complete is True:
            self._connect_complete = False

        return event

    def getExportEvent(self):
        event = self._exportStart

        if(self._exportStart is True):
            self._exportStart = False
        
        return event
    
    def getScanEvent(self):
        event = self._scanned_successfully

        if (self._scanned_successfully is True):
            self._scanned_successfully = False
        
        return event

    def getHomeCompleted(self):
        if self.motorHandler.get_homing_state() == 'done':
            return True
        else:
            return False

    def getExportCompleted(self):
        event = self._export_success

        if(self._export_success is True):
            self._export_success = False
        
        return event
    
    def listEvents(self):
        for key, value in self._events.items():
            print(str(key) + ": " + str(value))
        
if __name__ == "__main__":
    bsApp = BabyScannerApp(True, True)
    bsApp.start()