# ---- GENERAL MOTOR SETTINGS ------

microsteps = 16 
degrees_per_step_motor_x = 1.8 / microsteps # [deg/step]

rev_per_step_motor_x = degrees_per_step_motor_x/360 #[deg/step] / [deg/rev] = [rev/step]

gear_ratio_motor_x = 1 
gear_ratio_final_x = 40      # [mm / rev] (wheel pitch)


distance_per_step_x = rev_per_step_motor_x * gear_ratio_motor_x * gear_ratio_final_x  #  [mm / step]

# Position limits 
x_limits = [0, 600]
