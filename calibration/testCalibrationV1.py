import numpy as np
import cv2 as cv
import glob

#------------------------------------------Calibration Code--------------------------------------------------------------------------------
# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
boardRow = 6
boardCol = 9

objp = np.zeros((boardRow*boardCol,3), np.float32)
objp[:,:2] = np.mgrid[0:boardCol,0:boardRow].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob('*.jpeg')
for fname in images:
    img = cv.imread(fname)
    #kernel_sharpening = np.array([[-1,-1,-1],[-1, 9,-1],[-1,-1,-1]])
    #img = cv.filter2D(img2, -1, kernel_sharpening)
    #cv.imshow('sharpened', img)
    #cv.waitKey(200)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    #cv.imshow('sharpenedGREY', gray)
    #cv.waitKey(200)
    print('hello1')
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (boardCol,boardRow), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        print('Hello2')
        objpoints.append(objp)
        corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)
        # Draw and display the corners
        cv.drawChessboardCorners(img, (boardCol,boardRow), corners2, ret)  #4,4
        cv.imshow('img', img)
        cv.waitKey(1000)
        #if key == 27:
         #   cv.destroyAllWindows()
         #   break
cv.destroyAllWindows()
ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

#f = open("paramMTX.txt", "w+")
#f.write(mtx)
#f.close()

#f = open("paramDIST.txt", "w+")
#f.write(dist)
#f.close()

np.save('paramMTX.npy', mtx)
np.save('paramDIST.npy', dist)
#d = np.load('paramMTX.npy')
#mtx == d
#print(mtx)

#np.save(outfile, mtx)
#outfile.seek(0)

print('ret',ret)
print('mtx',mtx)
print('dist',dist)
print('rvecs',rvecs)
print('tvecs',tvecs)

img = cv.imread('nnfoto3.jpeg')
print(img)
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))

#--------------------------------------------------------------------------------------------------------------------------

#----------------------------------Code for frame with the calibration components-----------------------------------------
# undistort
mapx, mapy = cv.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w,h), 5)
dst = cv.remap(img, mapx, mapy, cv.INTER_LINEAR)
print('dst', dst)
# crop the image
cv.imshow('CalibrationNONCROP', dst)
x, y, w, h = roi
dst = dst[y:y+h, x:x+w]

cv.imshow('Calibration', dst)
cv.imshow('nfoto1', img)
while 1:
        k = cv.waitKey(33)
        if k == 27:
                cv.destroyAllWindows()
                break
#cv.imwrite('calibresult.png', dst)
