import numpy as np
import cv2 as cv
import glob

import matplotlib.pyplot as plt
import csv
import math

#Deze parameters komen uit testCalibrationV1.py
#De bovengenoemde programma genereert de parameters van de te kaliberen camera
mtx = np.load('paramMTX.npy')
dist = np.load('paramDIST.npy')
#----------------------------------------------------------
#img = cv.imread('left11.jpg') #Hierin moet de frame ingeladen worden, die verkregen wordt door de camera
#print(img)
img = cv.imread('nnfoto2.jpeg')
print(img) 
cv.imshow('Foto', img)
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
# undistort
mapx, mapy = cv.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w,h), 5)
dst = cv.remap(img, mapx, mapy, cv.INTER_LINEAR)
#---------------------------------------------------------------
print(dst)
# crop the image
cv.imshow('CalibrationImage', dst)
#x, y, w, h = roi
#st = dst[y:y+h, x:x+w]

while 1:
        k = cv.waitKey(33)
        if k == 27:
                cv.destroyAllWindows()
                break