from threading import Thread, Lock
import cv2

class CameraStream:

    def __init__(self, src = 0, width = 1280, height = 720):
        self.stream = cv2.VideoCapture(src)  

        self.stream.set(3, width)
        self.stream.set(4, height)

        self.stream.set(cv2.CAP_PROP_FPS, 90)

        self.setBrightness(0.28)
        self.setContrast(0.9)

        print(self.stream.get(cv2.CAP_PROP_FPS))
        (self.grabbed, self.frame) = self.stream.read()
        self.started = False
        self.read_lock = Lock()

    def start(self):
        if self.started:
            print ("Already started!")
            return None
        self.started = True
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()
        return self
    
    def update(self):
        while self.started:
            (grabbed, frame) = self.stream.read()
            self.read_lock.acquire()
            self.grabbed, self.frame = grabbed, frame
            self.read_lock.release()
    
    def read(self):
        self.read_lock.acquire()
        frame = self.frame.copy()
        self.read_lock.release()
        return frame
    
    def stop(self):
        self.started = False
        self.thread.join()

    def setContrast(self, con):
        self.stream.set(cv2.CAP_PROP_CONTRAST, con)
    
    def getContrast(self):
        return self.stream.get(cv2.CAP_PROP_CONTRAST)
    
    def setBrightness(self, br):
        self.stream.set(cv2.CAP_PROP_BRIGHTNESS, br)
    
    def getBrightness(self):
        return self.stream.get(cv2.CAP_PROP_BRIGHTNESS)

    def setFrame(self, w, h):
        self.stream.set(3, w)
        self.stream.set(4, h)
    
    def saveImage(self, name):
        frame = self.read()
        cv2.imwrite("/home/pi/" + name + ".jpeg",frame)

    def saveInputImage(self, img):
        cv2.imwrite("/home/pi/t3.jpeg", img)

    def __exit__(self, exc_type, exc_value, traceback):
        self.stream.release()