import time
from threading import Thread

from .serialcomm import SerialConnector
from .message import MessageStruct

def timeout(interval):
    t = time.perf_counter()

    while True:
        if(time.perf_counter() -t) > interval:
            yield True
        else:
            yield False

class MotorController:

    def __init__(self, port, baudrate):

        self.target = 0
        self._gear_x = 1
        self.move_type = 1

        self._homed = False
        self._home_state = {'status': 'required'}
        self._home_timeout = 30
        self._home_break = False

        self._sercom = SerialConnector(port, baudrate, MessageStruct(), MessageStruct())

    def connect(self):
        self._sercom.start_autoconnect()
        time.sleep(0.1)
        self.force_connection()

    def force_connection(self):
        connection = False

        i = 1
        while connection is False:
            self._sercom.ser.write(b'\x0a')
            time.sleep(0.1)
            
            self.set_position(i)
            time.sleep(0.1)
            pos = self.get_pos()
            print(self._sercom.rx)
            if pos == i:
                print('connection established')
                self.set_position(0)
                connection = True

            i += 1
        
    def isConnected(self):
        return self._sercom.connected
    
    def disconnect(self):
        self._sercom.disconnect()
    
    def set_gear(self, x):
        self._gear_x = x
    
    def get_gear(self):
        return self._gear_x
    
    def steps_to_metric(self, x, xv):
        x /= self._gear_x
        xv /= self._gear_x

        return (x,xv)
    
    def metric_to_steps(self, x, xv):
        x *= self._gear_x
        
        return(int(x),int(xv))
    
    def set_position(self, x = None, method = 'steps'):

        if method == 'metric':
            x = x*self._gear_x if not x is None else None
        
        if not x is None:
            self._sercom.tx.pos = int(x)
    
        else:
            self._sercom.tx.pos = self._sercom.rx.pos
        
        self._sercom.tx.cmd_type = 6
        self._sercom.send()

    def set_home(self):
        self.set_position(x = 0)
    
    def set_acc(self, acc):
        self._sercom.tx.acc = acc

        self._sercom.tx.cmd_type = 2
        self._sercom.send()
    
    def move(self, x, xv = 255):
        self._sercom.tx.pos = x
        self._sercom.tx.vel = xv if xv else 255

        self._sercom.tx.cmd_type = self.move_type

        self.target = x

        self._sercom.send()
    
    def _check_endstops(self):
        return self._sercom.rx.endstops
    
    def set_homing_timeout(self, seconds):
        self._home_timeout = seconds

    def start_homing_routine(self):
        if self._home_state['status'] != 'busy':
            self._home_state['status'] = 'start'
        
            t = Thread(target = self.find_home)
            t.start()
    
    def get_homing_state(self):
        return self._home_state.get('status')
    
    def find_home(self):
        homed = False
        self.set_position(1000, method='metric')
        if self._home_state['status'] == 'start':

            self.jog_metric(-1000, 20)
            self._home_break = False
            self._home_state['status'] = 'busy'
        
        if self._home_state['status'] == 'busy':
            to = timeout(self._home_timeout)
            next(to)

            while not next(to):
                end_triggered = self._check_endstops()
                if end_triggered != 0:
                    self.set_position(x = 0, method = 'metric')
                    self._home_state['status'] = 'done'
                    homed = True

                    return homed
                
                if self._home_break:
                    break
                time.sleep(1)
            
            if next(to):
                self._home_state['status'] = 'timeout'
                self.set_home()
        
        if self._home_break:
            self.set_home()
            self._home_state['status'] = 'required'
            homed = False

        return homed 
    
    def send_home(self, *args):
        self.move(0,*args)
    
    def jog(self,x,xv=255):
        x += self._sercom.rx.pos
        self.move(x,xv)
    
    def jog_metric(self, *args):
        self.jog(*self.metric_to_steps(*args))

    def move_metric(self, *args):
        self.move(*self.metric_to_steps(*args))
    
    def get_pos(self):
        return self._sercom.rx.pos
    
    def get_pos_metric(self):
        x = 1/self._gear_x * self._sercom.rx.pos

        return x
    
    def on_target(self):
        return self.get_pos() == self.target
    
    def block_until_target(self):

        while not self.on_target():
            time.sleep(0.01)
