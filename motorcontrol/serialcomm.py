import serial
import time
from threading import Thread
from collections import deque
from blinker import signal

class SerialConnector:

    def __init__(self, port, baudrate, rx_struct, tx_struct):
        self.port = port
        self.baudrate = baudrate
        self.ser = serial.Serial(port = None, baudrate = baudrate)

        self.ser.setDTR(False)
        self.rx = rx_struct
        self.tx = tx_struct
        self.header = b'\xff\xff'

        self.last_recv = (0,b'')
        self.queue = deque(maxlen = 1000)

        self._do_recv = False
        self._int_autoconnect = False

        self.on_data = signal('new_serial_data')
        self.connected = False

        self.t = Thread(target=self._autoconnect_routine, args=(2,))

    # Starts autoconnect routine in a thread with 2 second interval
    def start_autoconnect(self):
        self.t.start()
    
    # Autoconnect routine which tries to reconnect to serial port on a given interval
    def _autoconnect_routine(self, interval):
        i = 0
        self._int_autoconnect = False

        while not self.connected and not self._int_autoconnect:
            if self.connect():
                break
            else:
                time.sleep(interval)
            i += 1
        
        if not self._int_autoconnect:
            print('re-connected after %i attempts' %i)
        else:
            print('stopped autoconnecting')
        
    def connect(self):
        if self.ser.isOpen():
            self.ser.close()
        
        self.ser.port = self.port
        try:
            self.ser.open()
            self.connected = self.ser.isOpen()

            self.start_recv()
            print('connected on %s' %self.port)
        
        except serial.SerialException:
            self.connected = False
        
        return self.connected
    
    def data_available(self):
        if self.connected:
            return self.ser.in_waiting>0
        else:
            return False
    
    def disconnect(self):
        self.stop_recv()

        if self.ser.isOpen():
            self.ser.close()
        
        self.connected = False
        self._int_autoconnect = True
    
    def set_header(self, header):
        self.header = header
    
    def _receiver(self):
        self._do_recv = True
        try:

            # first read until first header
            self.ser.read_until(self.header)
            print('starting data receiver')

            while self._do_recv:
                data = self.header + self.ser.read_until(self.header).rstrip(self.header)
                t = time.time()

                self.last_recv = (t, data)
                self.queue.append((t,data))
                self.on_data.send()
                try:
                    self.rx.set_from_bytes(data)
                except ValueError:
                    pass
        
        except serial.SerialException:
            self.disconnect()
        
        print('exiting data receiver')
    
    def send(self):
        self.tx.set_checksum()
 
        self.ser.write(bytes(self.tx))
    
    def start_recv(self):
        if self._do_recv:
            self._do_recv = False
            time.sleep(0.1)
        
        self.queue = deque(maxlen = self.queue.maxlen)

        #start new thread
        self._recv_thread = Thread(target=self._receiver,args=())
        self._recv_thread.start()
    
    def stop_recv(self):
        self._do_recv = False
        self.t.join()
    
    def last_time(self):
        return time.time() - self.last_recv[0]