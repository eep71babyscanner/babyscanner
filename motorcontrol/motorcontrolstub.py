import time

class MotorController:

    def __init__(self, port, baudrate):
        self.port = port
        self.baudrate = baudrate

        self.target = 0
        self._gear_x = 1

        self._homed = False
        self._home_state = {'status': 'required'}
        self._home_timeout = 30
        self._home_break = False

        self._connected = False

        self._position = 0
    
    def connect(self):
        self._connected = True
    
    def isConnected(self):
        return self._connected

    def get_homing_state(self):
        return self._home_state
        
    def disconnect(self):
        self._connected = False
    
    def metric_to_steps(self, x, xv):
        x *= self._gear_x
    
    def set_gear(self, x):
        self._gear_x = x
        
    def set_position(self, x = None, method = 'steps'):
        
        if method == 'metric':
            x = x*self._gear_x if not x is None else None
        
        if not x is None:
            self._position = int(x)
        else:
            self._position = self._position
        
    def set_home(self):
        self.set_position(x=0)
    
    def move_metric(self, x, xv=255):
        pass
    
    def move(self, x, xv):
        pass
        
    def on_target(self):
        return True
