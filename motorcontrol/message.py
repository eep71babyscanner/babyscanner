import ctypes as ct

header_t = ct.c_ushort
footer_t = ct.c_ubyte
payload_length_t = ct.c_ubyte

class StatusMessage(ct.Structure):
    _pack_ = 1
    _fields_ = [ 
        ('end_stop_left', ct.c_uint8, 1),
        ('end_stop_right', ct.c_uint8, 1),
        ('error_code', ct.c_uint8, 6)
    ]

class MessageStruct(ct.Structure):
    _pack_ = 1
    _fields_ = [
        ('header', header_t),
        ('payload_length', payload_length_t),
        ('cmd_type', ct.c_uint16),
        ('pos', ct.c_int32),
        ('vel', ct.c_uint16),
        ('endstops', ct.c_uint16),
        ('footer', footer_t)
    ]

    def __init__(self):
        self.header_len = ct.sizeof(ct.c_short)
        self.footer_len = ct.sizeof(ct.c_ubyte)

        self._start_index = ct.sizeof(ct.c_short) 
        self._end_index = ct.sizeof(self) - ct.sizeof(ct.c_ubyte)
        self.header = 255<<8|255
        self.payload_length = ct.sizeof(self) - self.header_len - ct.sizeof(ct.c_ubyte) - self.footer_len

    def set_checksum(self):
        self.footer = 256 - sum(self.get_crc_data())%256
        return self.footer

    def get_crc_data(self):
        return bytes(self)[self._start_index:self._end_index]
    
    def check_checksum(self):
        self.get_crc_data()        
        return (sum(self.get_crc_data()) + self.footer) % 256 == 0

    def _to_dict(self):
        return {name:getattr(self,name) for name,typ in self._fields_}

    def to_list(self):
        return[getattr(self,name) for name,typ in self._fields_] 
    
    def set_from_bytes(self, bytestr, do_checksum = True):
        old_bytes = bytes(self)

        ct.memmove(ct.addressof(self), bytestr, len(bytestr))
        
        if not self.check_checksum() and do_checksum:
            self.set_from_bytes(old_bytes,False)
            
            raise ValueError('checksum mismatch')
    
    def to_csv_line(self,sep = '\t'):
        out = ''

        for item in self.to_list():
            out += str(item) + sep    
        
        return out + '\n'
    
    def __repr__(self):
        return '<message struct> : ' + self._to_dict().__repr__()
