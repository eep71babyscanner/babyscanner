import sys
from PyQt5 import QtCore, QtGui, QtWidgets

def setupStatusBar(self, incommingWindow, function):
    """Shows the statusbar, depends on a mainWindow"""
    self.theWindow = incommingWindow
    self.Widget = QtWidgets.QWidget(self.centralwidget)
    self.Widget.setGeometry(QtCore.QRect(0, 0, 800, 40))
    self.Widget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:1, stop:0 rgb(94, 106, 255, 255), stop:0.880682 rgba(255, 255, 255, 255))")
    self.Widget.setObjectName("Widget")
    self.thisStatusBar = QtWidgets.QStatusBar(self.theWindow)
    self.thisStatusBar.setObjectName("incommingWindow")
    self.theWindow.setStatusBar(self.thisStatusBar)
    #self.menuBar = QtWidgets.QWidget(self.centralwidget)
    #self.menuBar.setGeometry(QtCore.QRect(0, 0, 800, 20))
    #self.menuBar.setStyleSheet("background-color: rgb(94, 106, 255, 255)")
    
    self.DateTime = QtWidgets.QDateTimeEdit(self.Widget)
    self.DateTime.setDateTime(QtCore.QDateTime.currentDateTime())
    self.DateTime.setGeometry(QtCore.QRect(580, 5, 110, 22))
    self.DateTime.setFrame(False)
    self.DateTime.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
    self.DateTime.setObjectName("DateTime")
    font = QtGui.QFont()
    font.setPointSize(15)
    font.setFamily("High Tower Text")
    self.DateTime.setFont(font)    

    self.projectName = QtWidgets.QLabel(self.Widget)
    self.projectName.setGeometry(QtCore.QRect(400, 3, 200, 30))
    self.projectName.setStyleSheet("color: #000000; background-color: rgba(0,0,0,0%)")
    font = QtGui.QFont()
    font.setPointSize(15)
    font.setFamily("High Tower Text")
    font.setPointSize(15)
    self.projectName.setFont(font)
    self.projectName.setText("Babyscanner")

    #self.Widget2 = QtWidgets.QWidget(self.Widget)
    #self.Widget2.setGeometry(QtCore.QRect(580, 0, 100, 40))
    #self.Widget2.setStyleSheet("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:1, stop:0 rgb(94, 106, 255, 255), stop:0.880682 rgba(255, 255, 255, 255))")

    exitBtn = QtWidgets.QPushButton(self.Widget)
    exitBtn.setGeometry(QtCore.QRect(772, 0, 30, 33))
    exitBtn.setAcceptDrops(False)
    exitBtn.setAutoFillBackground(False)
    exitBtn.setStyleSheet("background-color: #FFFFFF; \n" ";font-size: 10pt;font-family: High Tower Text; color = #FFFFFF;")
    exitBtn.setText("X")


    stopFunction = function
    
    def click(self):
        if stopFunction != None:
            stopFunction()

        sys.exit(0)

    def push():
        exitBtn.setStyleSheet("background-color: #EB2F2F; \n" ";font-size: 10pt;font-family: High Tower Text; color = #FFFFFF;")

    def release():
        exitBtn.setStyleSheet("background-color: #FFFFFF; \n" ";font-size: 10pt;font-family: High Tower Text; color = #FFFFFF;")

    exitBtn.pressed.connect(push)
    exitBtn.released.connect(release)
    exitBtn.clicked.connect(click)
