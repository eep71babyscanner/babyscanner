import sys
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
import statusBar

class MainWindow(object):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.theWindow = QtWidgets.QMainWindow()
        self.theWindow.setObjectName("theWindow")
        self.theWindow.resize(800, 480)
        self.window1()
        self.startScan.clicked.connect(self.scanStartedWindow)
        self.theWindow.show()

    def window1(self):
        self.centralWidget()
        self.cameraFrame()
        self.pushButtons()
        self.labels()
        statusBar.setupStatusBar(self, self.theWindow)
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.theWindow)

    def centralWidget(self):
        self.centralwidget = QtWidgets.QWidget(self.theWindow)
        self.theWindow.setStyleSheet("background-color: rgb(255, 255, 255)")
        self.centralwidget.setObjectName("centralwidget")
        self.theWindow.setCentralWidget(self.centralwidget)

    def labels(self):
        self.camPrevLbl = QtWidgets.QLabel(self.centralwidget)
        self.camPrevLbl.setGeometry(QtCore.QRect(440, 90, 141, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(15)
        self.camPrevLbl.setFont(font)
        self.camPrevLbl.setObjectName("camPrevLbl")

    def pushButtons(self):
        self.startScan = QtWidgets.QPushButton(self.centralwidget)
        self.startScan.setGeometry(QtCore.QRect(60, 240, 120, 50))
        self.startScan.setAcceptDrops(False)
        self.startScan.setAutoFillBackground(False)
        self.startScan.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" "")
        self.startScan.setObjectName("startScan")

    def cameraFrame(self):
        self.camFrame = QtWidgets.QLabel(self.centralwidget)
        self.camFrame.setGeometry(QtCore.QRect(260, 140, 480, 270))
        self.camFrame.setFrameShape(QtWidgets.QFrame.Box)
        self.camFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.camFrame.setMidLineWidth(0)
        self.camFrame.setText("")
        self.camFrame.setAlignment(QtCore.Qt.AlignCenter)
        self.camFrame.setObjectName("camFrame")

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.theWindow.setWindowTitle(_translate("theWindow", "theWindow"))
        self.startScan.setText(_translate("theWindow", "Start Scan"))
        self.camPrevLbl.setText(_translate("theWindow", "Camera preview"))

    def scanStartedWindow(self):
        exit()
        self.theWindow.hide()

    def showFrame(self, image):
        self.image = image
        self.size = image.size()
        self.painter = QtGui.QPainter()
        self.painter.begin(self)
        self.painter.drawImage(QtCore.QPoint(0, 0), self.image)
        self.painter.end()



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())