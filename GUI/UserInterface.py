import sys
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
from .mainWindow import Ui_MainWindow
import time 
from threading import Thread 

class UserInterface():
    "Ties all pages together"
    def __init__(self):
        super(UserInterface, self).__init__()
        self.app = QtWidgets.QApplication(sys.argv)
        self.window = Ui_MainWindow()
        self.status = "IDLE"
        self.image = None

    def start(self):
        self.window.start()
        sys.exit(self.app.exec_())

    def setLength(self, length):
        self.window.setLength(length)
    
    def setStatus(self, status):
        self.status = status
        self._updateStatus()
    
    def getStartEvent(self):
        if self.window.name == 'main':
            return self.window.getStartEvent()
        else:
            return False
        
    def getStopEvent(self):
        if self.window.name == 'main':
            return self.window.getStopEvent()
        else:
            return False

    def getHomeEvent(self):
        if self.window.name == 'main':
            return self.window.getHomeEvent()
        else:
            return False
    
    def getBrightnessLowerEvent(self):
        if self.window.name == 'main':
            return self.window.getBrightnessLower()
        else:
            return False
    def getBrightnessHigherEvent(self):
        if self.window.name == 'main':
            return self.window.getBrightnessHigher()
        else:
            return False    
    def getContrastLowerEvent(self):
        if self.window.name == 'main':
            return self.window.getContrastLower()
        else:
            return False    
    def getContrastHigherEvent(self):
        if self.window.name == 'main':
            return self.window.getContrastHigher()
        else:
            return False 
           
    def getLaserHigherEvent(self):
        if self.window.name == 'main':
            return self.window.getLaserHigher()
        else:
            return False    
    def getLaserLowerEvent(self):
        if self.window.name == 'main':
            return self.window.getLaserLower()
        else:
            return False        
    def _updateStatus(self):
        self.window.showEvent(self.status)