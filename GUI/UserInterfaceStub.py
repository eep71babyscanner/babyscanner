class UserInterface():

    def __init__(self):
        self.window = WindowStub('main')
        self.status = 'idle'
    def start(self):
        pass 
    def setStatus(self, status):
        self.status = status

    def getStartEvent(self):
        return False

    def getStopEvent(self):
        return False

    def getHomeEvent(self):
        return False

    def getExportEvent(self):
        return False

    def _updateStatus(self):
        pass

class WindowStub():

    def __init__(self, name):
        self.name = name
        self.stopFunction = None

    def start(self):
        pass
    
    def setStopFunction(self, function):
        self.stopFunction = function
    
    def displayImage(self, img):
        pass