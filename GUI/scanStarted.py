import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from . import statusBar

class Ui_scanStarted():
    """Shows the scan started window"""
    def __init__(self):
        super(Ui_scanStarted, self).__init__()
        self.scanStarted = QtWidgets.QMainWindow()
        self.scanStarted.setObjectName("scanStarted")
        self.scanStarted.setGeometry(0, -1, 800, 510)
        self.scanStartedWindow()
        self.stopScan.clicked.connect(self.scanStopped)
        self.stopScan.pressed.connect(self.scanStoppedPressed)
        self.stopScan.released.connect(self.scanStoppedReleased)
        QtCore.QMetaObject.connectSlotsByName(self.scanStarted)

    def start(self):
        self.scanStarted.show()
    
    def scanStartedWindow(self):
        self.centralwidgets()
        self.labels()
        self.pushButtons()
        statusBar.setupStatusBar(self, self.scanStarted)

    def centralwidgets(self):
        self.centralwidget = QtWidgets.QWidget(self.scanStarted)
        self.scanStarted.setStyleSheet("background-color: rgb(255, 255, 255)")
        self.centralwidget.setObjectName("centralwidget")
        self.scanStarted.setCentralWidget(self.centralwidget)

    def labels(self):
        self.message1 = QtWidgets.QLabel(self.centralwidget)
        self.message1.setGeometry(QtCore.QRect(440, 200, 171, 31))
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.message1.setFont(font)
        self.message1.setText("Bezig met scannen")
        self.message2 = QtWidgets.QLabel(self.centralwidget)
        self.message2.setGeometry(QtCore.QRect(440, 250, 171, 31))
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.message2.setFont(font)
        self.message2.setText("Even geduld a.u.b.")

    def pushButtons(self):
        self.stopScan = QtWidgets.QPushButton(self.centralwidget)
        self.stopScan.setGeometry(QtCore.QRect(60, 250, 120, 50))
        self.stopScan.setAcceptDrops(False)
        self.stopScan.setAutoFillBackground(False)
        self.stopScan.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")
        self.stopScan.setText("Stop scan")

    def scanDone(self, finished):
        if finished == True:
            from scanFinished import Ui_scanFinished
            self.UI = Ui_scanFinished()
            self.scanStarted.hide()

    def scanStopped(self):
        #verstuur bericht naar Kai voor stoppen scan
        from scanFinished import Ui_scanFinished
        self.Ui = Ui_scanFinished()
        self.scanStarted.hide()

    def scanStoppedPressed(self):
        self.stopScan.setStyleSheet("background-color: #2635FF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")

    def scanStoppedReleased(self):
        self.stopScan.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")


