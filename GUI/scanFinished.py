import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from . import statusBar

class Ui_scanFinished():
    """Shows the scan finished window"""
    def __init__(self):
        super(Ui_scanFinished, self).__init__()
        self.name = 'scan'
        self.scanFinished = QtWidgets.QMainWindow()
        self.scanFinished.setObjectName("scanFinished")
        self.scanFinished.setGeometry(0, -1, 800, 510)
        self.scanFinished.setFixedSize(800, 510)
        self.stopFunction = None
    
    def setStopFunction(self, function):
        self.stopFunction = function

    def start(self):
        self._scanFinishedWindow()
        self.scanFinished.show()

    def _scanFinishedWindow(self):
        self._centralWidget()
        self._pushButtons()
        self._buttonInit()
        self.scanCompletePicture()
        self._labels()
        statusBar.setupStatusBar(self, self.scanFinished, self.stopFunction)

    def _buttonInit(self):
        #self.exportToSTL.clicked.connect(self._exportToSTLbtnClicked)
        self.backToMain.clicked.connect(self._backToMainbtnClicked)
        
        #self.exportToSTL.pressed.connect(self._exportToSTLbtnPressed)
        self.backToMain.pressed.connect(self._backToMainbtnPressed)
        
        #self.exportToSTL.released.connect(self._exportToSTLbtnReleased)
        self.backToMain.released.connect(self._backToMainbtnReleased)

    def _centralWidget(self):
        self.centralwidget = QtWidgets.QWidget(self.scanFinished)
        self.scanFinished.setStyleSheet("background-color: rgb(255, 255, 255)")
        self.centralwidget.setObjectName("centralwidget")
        self.scanFinished.setCentralWidget(self.centralwidget)

    def scanCompletePicture(self):
        self.scanFrame = QtWidgets.QLabel(self.scanFinished)
        self.scanFrame.setGeometry(QtCore.QRect(260, 140, 480, 270))
        self.scanFrame.setFrameShape(QtWidgets.QFrame.Box)
        self.scanFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.scanFrame.setMidLineWidth(0)
        self.scanFrame.setText("")
        self.scanFrame.setAlignment(QtCore.Qt.AlignCenter)
        self.scanFrame.setObjectName("scanFrame")
        self.scanFrame.setStyleSheet("border: 0px")
        
        #self._showMatPlotLib()
        self.pixmap = QtGui.QPixmap("/home/pi/hoogteprofiel.png")
        self.showImage(self.pixmap)

    def _pushButtons(self):
        #self.exportToSTL = QtWidgets.QPushButton(self.scanFinished)
        #self.exportToSTL.setGeometry(QtCore.QRect(60, 205, 120, 50))
        #self.exportToSTL.setAcceptDrops(False)
        #self.exportToSTL.setAutoFillBackground(False)
        #self.exportToSTL.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")
        #self.exportToSTL.setText("Export to STL")

        self.backToMain = QtWidgets.QPushButton(self.scanFinished)
        self.backToMain.setGeometry(QtCore.QRect(60, 295, 120, 50))
        self.backToMain.setAcceptDrops(False)
        self.backToMain.setAutoFillBackground(False)
        self.backToMain.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")
        self.backToMain.setText("Back")

    def _labels(self):
        self.scanFramelbl = QtWidgets.QLabel(self.scanFinished)
        self.scanFramelbl.setGeometry(QtCore.QRect(450, 90, 141, 31))
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.scanFramelbl.setFont(font)
        self.scanFramelbl.setText("Preview")
        #self.length(200)

    def length(self, babylength):
        #Ontvang lengte baby van kai
        self.displayLength = QtWidgets.QLabel(self.scanFinished)
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.displayLength.setFont(font)
        self.displayLength.setText("The length of the baby is: " + str(babylength) + " mm")
        self.displayLength.adjustSize()
        self.displayLength.move(300, 420)

#    def scanButton(self):
#       if self.scanStarted == True:
#            self.scanStarted = False
#            return True
#        else:
#            return False

    def showImage(self, img):
        #Krijg een image binnen
        self.pixmap = img
        self.pixmap = self.pixmap.scaled(self.scanFrame.width(), self.scanFrame.height(), QtCore.Qt.KeepAspectRatio)
        self.scanFrame.setPixmap(self.pixmap)
        self.scanFrame.setAlignment(QtCore.Qt.AlignCenter)
        self.image = img

    def showMatPlotLib(self, incommingPlot):
        self.frameLayout = QtWidgets.QVBoxLayout(self.scanFrame)
        self.matPlotLib = FigureCanvas(Figure(figsize=(5, 3)))
        self.frameLayout.addWidget(self.matPlotLib)

        self.thePlot = self.matPlotLib.figure.subplots()
        #self.lineSpace = np.linspace(0, 10, 501)
        self.thePlot.plot(incommingPlot)

    #def _exportToSTLbtnClicked(self):
        #self._exportToStlBtn = True

    #def getExportEvent(self):
        #event = self._exportToStlBtn

        #if (self._exportToStlBtn is True):
        #    self._exportToStlBtn = False
        
        #return event
    
    def _backToMainbtnClicked(self):
        self.scanFinished.hide()

    def _backToMainbtnPressed(self):
        self.backToMain.setStyleSheet("background-color: #2635FF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")

    def _backToMainbtnReleased(self):
        self.backToMain.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")

    #def _exportToSTLbtnPressed(self):
        #self.exportToSTL.setStyleSheet("background-color: #2635FF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")

    #def _exportToSTLbtnReleased(self):
        #self.exportToSTL.setStyleSheet("background-color: #5E6AFF; \n" "border-radius: 15px;\n" ";font-size: 10pt;font-family: High Tower Text;")
