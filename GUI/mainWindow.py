import sys
import cv2 #pip install opencv-python
from . import statusBar
import threading
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow():
    """Shows the main window"""
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.name = 'main'
        self.theWindow = QtWidgets.QMainWindow()
        self.theWindow.setObjectName("theWindow")
        self.theWindow.setGeometry(0, -1, 800, 510)
        self.theWindow.setFixedSize(800, 510)
        self.showResultBtn = False
        self.scanStartedBtn = False
        self.scanStoppedBtn = False
        self.getHomeBtn = False
        self.scanStartedButtonClicked = False
        self.scanStoppedButtonClicked = True
        self.getHomeButtonClicked = False

        self.centralwidget = QtWidgets.QWidget(self.theWindow)

        self.stopFunction = None
    
    def setStopFunction(self, function):
        self.stopFunction = function

    def start(self):
        self._mainWindow()
        self.theWindow.show()

    def _mainWindow(self):
        self._centralWidget()
        self._cameraFrame()
        self._pushButtons()
        self._labels()
        #self._getFrame()
        statusBar.setupStatusBar(self, self.theWindow, self.stopFunction)
        QtCore.QMetaObject.connectSlotsByName(self.theWindow)
        self._buttonInit()
        self.showEvent("Connecting")

    def _buttonInit(self):
        self.startScan.clicked.connect(self._scanStartedClicked)
        self.stopScan.clicked.connect(self._stopScanClicked)
        self.getHome.clicked.connect(self._getHomeClicked)
        self.showResult.clicked.connect(self._showResultClicked)
        self._brightnessLower.clicked.connect(self._brightnessLowerClicked)
        self._brightnessHigher.clicked.connect(self._brightnessHigherClicked)
        self._contrastLower.clicked.connect(self._contrastLowerClicked)
        self._contrastHigher.clicked.connect(self._contrastHigherClicked)
        self._laserLower.clicked.connect(self._laserLowerClicked)
        self._laserHigher.clicked.connect(self._laserHigherClicked)

        self.startScan.pressed.connect(self._scanStartedPressed)
        self.stopScan.pressed.connect(self._stopScanPressed)
        self.getHome.pressed.connect(self._getHomePressed)
        self.showResult.pressed.connect(self._showResultPressed)

        self.startScan.released.connect(self._scanStartedReleased)
        self.stopScan.released.connect(self._stopScanReleased)
        self.getHome.released.connect(self._getHomeReleased)
        self.showResult.released.connect(self._showResultReleased)

    def _centralWidget(self):
        self.theWindow.setStyleSheet("background-color: rgb(255, 255, 255)")
        self.centralwidget.setObjectName("centralwidget")
        self.theWindow.setCentralWidget(self.centralwidget)

    def _labels(self):
        self.camPrevLbl = QtWidgets.QLabel(self.centralwidget)
        self.camPrevLbl.setGeometry(QtCore.QRect(440, 110, 180, 31))
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.camPrevLbl.setFont(font)
        self.camPrevLbl.setText("Camera preview")

        self.eventLbl = QtWidgets.QLabel(self.centralwidget)
        self.eventLbl.setGeometry(QtCore.QRect(60, 90, 300, 31))
        font = QtGui.QFont()
        font.setFamily("High Tower Text")
        font.setPointSize(18)
        self.eventLbl.setFont(font)

        self._brightness = QtWidgets.QLabel(self.centralwidget)
        self._brightness.setGeometry(QtCore.QRect(145, 165, 100, 25))
        self._font = QtGui.QFont()
        self._font.setFamily("High Tower Text")
        self._font.setPointSize(15)
        self._brightness.setFont(self._font)
        self._brightness.setText("Brightness")

        self._contrast = QtWidgets.QLabel(self.centralwidget)
        self._contrast.setGeometry(QtCore.QRect(145, 255, 100, 25))
        self._font = QtGui.QFont()
        self._font.setFamily("High Tower Text")
        self._font.setPointSize(15)
        self._contrast.setFont(self._font)
        self._contrast.setText("Contrast")

        self._laser = QtWidgets.QLabel(self.centralwidget)
        self._laser.setGeometry(QtCore.QRect(145, 340, 100, 25))
        self._font = QtGui.QFont()
        self._font.setFamily("High Tower Text")
        self._font.setPointSize(15)
        self._laser.setFont(self._font)
        self._laser.setText("Laser")

    def showEvent(self, givenEvent):
        self.eventLbl.setText("Status: " + givenEvent)
        self.eventLbl.show()

    def _pushButtons(self):
        self.startScan = QtWidgets.QPushButton(self.centralwidget)
        self.startScan.setGeometry(QtCore.QRect(20, 194, 80, 50))
        self.startScan.setAutoFillBackground(False)
        self.startScan.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self.startScan.setText("Start Scan")

        self.stopScan = QtWidgets.QPushButton(self.centralwidget)
        self.stopScan.setGeometry(QtCore.QRect(20, 258, 80, 50))
        self.stopScan.setAutoFillBackground(False)
        self.stopScan.setStyleSheet("background-color: #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self.stopScan.setText("Stop Scan")

        self.getHome = QtWidgets.QPushButton(self.centralwidget)
        self.getHome.setGeometry(QtCore.QRect(20, 322, 80, 50))
        self.getHome.setAutoFillBackground(False)
        self.getHome.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self.getHome.setText("Get module home")

        self.showResult = QtWidgets.QPushButton(self.centralwidget)
        self.showResult.setGeometry(QtCore.QRect(20, 386, 80, 50))
        self.showResult.setAutoFillBackground(False)
        self.showResult.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self.showResult.setText("Show results")

        self._brightnessLower = QtWidgets.QPushButton(self.centralwidget)
        self._brightnessLower.setGeometry(QtCore.QRect(130, 194, 50, 50))
        self._brightnessLower.setAutoFillBackground(False)
        self._brightnessLower.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._brightnessLower.setText("<")

        self._brightnessHigher = QtWidgets.QPushButton(self.centralwidget)
        self._brightnessHigher.setGeometry(QtCore.QRect(200, 194, 50, 50))
        self._brightnessHigher.setAutoFillBackground(False)
        self._brightnessHigher.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._brightnessHigher.setText(">")

        self._contrastLower = QtWidgets.QPushButton(self.centralwidget)
        self._contrastLower.setGeometry(QtCore.QRect(130, 280, 50, 50))
        self._contrastLower.setAutoFillBackground(False)
        self._contrastLower.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._contrastLower.setText("<")

        self._contrastHigher = QtWidgets.QPushButton(self.centralwidget)
        self._contrastHigher.setGeometry(QtCore.QRect(200, 280, 50, 50))
        self._contrastHigher.setAutoFillBackground(False)
        self._contrastHigher.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._contrastHigher.setText(">")

        self._laserLower = QtWidgets.QPushButton(self.centralwidget)
        self._laserLower.setGeometry(QtCore.QRect(130, 366, 50, 50))
        self._laserLower.setAutoFillBackground(False)
        self._laserLower.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._laserLower.setText("<")

        self._laserHigher = QtWidgets.QPushButton(self.centralwidget)
        self._laserHigher.setGeometry(QtCore.QRect(200, 366, 50, 50))
        self._laserHigher.setAutoFillBackground(False)
        self._laserHigher.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
        self._laserHigher.setText(">")

    def _cameraFrame(self):
        self.camFrame = QtWidgets.QLabel(self.centralwidget)
        self.camFrame.setGeometry(QtCore.QRect(260, 180, 480, 270))
        self.camFrame.setFrameShape(QtWidgets.QFrame.Box)
        self.camFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.camFrame.setMidLineWidth(0)
        self.camFrame.setText("")
        self.camFrame.setAlignment(QtCore.Qt.AlignCenter)
        self.camFrame.setObjectName("camFrame")
        self.camFrame.setStyleSheet("border-radius: 0px")

    def getStartEvent(self):
        event = self.scanStartedBtn
        if self.scanStartedBtn:
            self.scanStartedBtn = False
        return event

    def getStopEvent(self):
        event = self.scanStoppedBtn
        if self.scanStoppedBtn:
            self.scanStoppedBtn = False
        return event

    def getHomeEvent(self):
        event = self.getHomeBtn
        if self.getHomeBtn:
            self.getHomeBtn = False
        return event
    
    def _getScanStarted(self):
        event = self.scanStartedButtonClicked
        return event

    def _getScanStopped(self):
        event = self.scanStoppedButtonClicked
        return event

    def _getScanHome(self):
        event = self.getHomeButtonClicked
        return event

    def _brightnessLowerClicked(self):
        self._brightnessHigherBtn = True

    def _brightnessHigherClicked(self):
        self._brightnessLowerBtn = True

    def _contrastLowerClicked(self):
        self._contrastHigherBtn = True

    def _contrastHigherClicked(self):
        self._contrastLowerBtn = True

    def _laserHigherClicked(self):
        self._laserHigherBtn = True

    def _laserLowerClicked(self):
        self._laserLowerBtn = True

    def _getHomeClicked(self):
        self.scanPushed = self._getScanStarted()
        self.homePushed = self._getScanHome()
        if not self.scanPushed and not self.homePushed:
            self.getHomeBtn = True
            self.scanStartedButtonClicked = False
            self.scanStoppedButtonClicked = False
            self.getHomeButtonClicked = True
            #self.showEvent("Getting Home")
            self.getHome.setStyleSheet("background-color: #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;") #Standard blue
            self.startScan.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;") #grey
            self.stopScan.setStyleSheet("background-color: #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _getHomePressed(self):
        self.scanPushed = self._getScanStarted()
        self.homePushed = self._getScanHome()
        if not self.scanPushed and not self.homePushed:
            self.getHome.setStyleSheet("background-color: #2635FF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _getHomeReleased(self):
        self.scanPushed = self._getScanStarted()
        self.homePushed = self._getScanHome()
        if not self.scanPushed and not self.homePushed:
            self.getHome.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _scanStartedClicked(self):
        self.scanPushed = self._getScanStarted()
        if not self.scanPushed:
            self._progressBar()
            self.scanStartedBtn = True
            self.scanStartedButtonClicked = True
            self.scanStoppedButtonClicked = False
            self.getHomeButtonClicked = False
            #self.showEvent("Scan started")
            self.startScan.setStyleSheet("background-color: #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
            self.stopScan.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
            self.getHome.setStyleSheet("background-color: #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _scanStartedPressed(self):
        self.scanPushed = self._getScanStarted()
        if not self.scanPushed:
            self.startScan.setStyleSheet("background-color: #2635FF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;") #clicked darker blue

    def _scanStartedReleased(self):
        self.scanPushed = self._getScanStarted()
        if not self.scanPushed:
            self.startScan.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _stopScanClicked(self):
        self.stopPushed = self._getScanStopped()
        self.homePushed = self._getScanHome()
        if not self.stopPushed and not self.homePushed:
            self.progressBar.hide()
            self.scanStoppedBtn = True
            self.scanStartedButtonClicked = False
            self.scanStoppedButtonClicked = True
            self.getHomeButtonClicked = False
            #self.showEvent("Scan stopped")
            self.startScan.setStyleSheet("background-color:  #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
            self.stopScan.setStyleSheet("background-color:  #959595; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")
            self.getHome.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _stopScanPressed(self):
        self.stopPushed = self._getScanStopped()
        self.homePushed = self._getScanHome()
        if not self.stopPushed and not self.homePushed:
            self.stopScan.setStyleSheet("background-color: #2635FF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _stopScanReleased(self):
        self.stopPushed = self._getScanStopped()
        self.homePushed = self._getScanHome()
        if not self.stopPushed and not self.homePushed:
            self.stopScan.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _showResultClicked(self):
        from GUI import scanFinished
        self.Ui = scanFinished.Ui_scanFinished()
        self.Ui.length(self.value)
        self.Ui.start()
        #self.theWindow.hide()
        #self.timer.stop()


    def _showResultPressed(self):
        self.showResult.setStyleSheet("background-color: #2635FF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def _showResultReleased(self):
        self.showResult.setStyleSheet("background-color: #5E6AFF; " "border-radius: 15px;" ";font-size: 10pt;font-family: High Tower Text;")

    def displayImage(self, img):
        qformat = QtGui.QImage.Format_Indexed8
        #qformat = QtGui.QImage.Format_RGBA8888
        qformat = QtGui.QImage.Format_RGB888
        outImage = QtGui.QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        outImage = outImage.rgbSwapped()
        self.camFrame.setPixmap(QtGui.QPixmap.fromImage(outImage))
        self.camFrame.setScaledContents(True)

    def _progressBar(self):
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget) 
        self.progressBar.setGeometry(60, 140, 220, 20)
        self.setProgress(10)
        self.progressBar.show()

    def setProgress(self, value):
        self.value = value
        self.progressBar.setValue(self.value)

    def getBrightnessHigher(self):
        event = self._brightnessHigherBtn
        if self._brightnessHigherBtn:
            self._brightnessHigherBtn = False
        return event

    def getBrightnessLower(self):
        event = self._brightnessLowerBtn
        if self._brightnessLowerBtn:
            self._brightnessLowerBtn = False
        return event

    def getContrastHigher(self):
        event = self._contrastHigherBtn
        if self._contrastHigherBtn:
            self._contrastHigherBtn = False
        return event

    def getContrastLower(self):
        event = self._contrastLowerBtn
        if self._contrastLowerBtn:
            self._contrastLowerBtn = False
        return event

    def getLaserHigher(self):
        event = self._laserHigherBtn
        if self._laserHigherBtn:
            self._laserHigherBtn = False
        return event

    def getLaserLower(self):
        event = self._laserLowerBtn
        if self._laserLowerBtn:
            self._laserLowerBtn = False
        return event

    def setLength(self, value):
        self.value = value


#************ BACKUP CODE********************

#    def update_frame(self):
#        vc = cv2.VideoCapture(0)
#        rval, img = vc.read()
#        self.showFrame(img)

#    def getFrame(self):
#        self.timer = QtCore.QTimer()
#        self.timer.timeout.connect(self.update_frame)
#        self.timer.start(5)

#    def showFrame(self, img):
#        self.rgbImage = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#        self.convertToQtFormat = QtGui.QImage(self.rgbImage.data, self.rgbImage.shape[1],
#            self.rgbImage.shape[0], QtGui.QImage.Format_RGB888)
#        self.convertToQtFormat = QtGui.QPixmap.fromImage(self.convertToQtFormat)
#        self.pixmap = QtGui.QPixmap(self.convertToQtFormat)
#        self.resizeImage = self.pixmap.scaled(480, 270, QtCore.Qt.KeepAspectRatio)
#        QtWidgets.QApplication.processEvents()
#        self.camFrame.setPixmap(self.resizeImage)
#        self.camFrame.show()
